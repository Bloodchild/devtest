﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankLibrary
{
    public class Current : IAccount
    {
        const decimal OVERDRAFT_MAXIMUM = 100000M;
     
        public decimal CurrentBalance { get; set; }
        public decimal OverdraftLimit { get; set; }
        public string AccountUniqueId { get; set; }

        public decimal Deposit(decimal amount)
        {
            CurrentBalance += amount;
            return CurrentBalance;
        }

        public decimal Withdraw(decimal amount)
        {
            if (CurrentBalance + OverdraftLimit < amount || (CurrentBalance < 0 && (CurrentBalance - amount) > -(OVERDRAFT_MAXIMUM + 1)))
                throw new Exception("you cannot withdraw more than your overdraft amount allows");
            CurrentBalance -= amount;
            return CurrentBalance;
        }
    }
}
