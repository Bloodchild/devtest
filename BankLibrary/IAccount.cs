﻿using System;

namespace BankLibrary
{
    public interface IAccount
    {
        decimal CurrentBalance { get; set; }
        string AccountUniqueId { get; set; }
        decimal Withdraw(decimal amount);
        decimal Deposit(decimal amount);
    }
}
