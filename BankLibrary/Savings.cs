﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankLibrary
{
    public class Savings : IAccount
    {
        public Savings(decimal initialDeposit)
        {
            if (initialDeposit < 1000)
                throw new Exception("You need at least R1000 to open an account with us");
            CurrentBalance = initialDeposit;
        }

        public decimal CurrentBalance { get; set; } = 1000;
        public string AccountUniqueId { get;set; }

        public decimal Deposit(decimal amount)
        {
             CurrentBalance += amount;
            return CurrentBalance;
        }

        public decimal Withdraw(decimal amount)
        {
            if (CurrentBalance - amount < 1000) throw new Exception("you have to have R1000 minimum in your account");
            CurrentBalance -= amount;
            return CurrentBalance;
        }
    }
}
