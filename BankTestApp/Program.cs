﻿using BankLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankTestApp
{
    class Program
    {
        private static int _state = 0;

        private static List<IAccount> Accounts = new List<IAccount>
        {
            new Savings(2000M){ AccountUniqueId= "1"},
            new Current(){ AccountUniqueId="2", CurrentBalance=1000, OverdraftLimit=25000}
        };

        static void Main(string[] args)
        {
            while (true)
            {
                GenerateUI(_state);
            }
        }
        public static void GenerateUI(int state)
        {
            if (state == 0)
            {
                InitialStateUi(state);
            }
            if (state == 1)
            {
                CreateAccountUi(state);
            }
            if (state == 2)
            {
                DepositIntoAccount();
            }
            if (state == 3)
            {
                WithdrawFromAccount();
            }
        }
        public static void InitialStateUi(int state)
        {
            if (state == 0)
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("1-----------CREATE ACUOUNT---------");
                Console.WriteLine("2--------DEPOSIT INTO ACCOUNT------");
                Console.WriteLine("3--------WITHDRAW FROM ACCOUNT-----");
                Console.WriteLine("-----------------------------------");
            }
            var stateSelected = Console.ReadLine();
            _state = int.TryParse(stateSelected.ToString(), out _state) ? int.Parse(stateSelected.ToString()) : throw new Exception("please Input only the number");
        }
        public static void CreateAccountUi(int state)
        {
            decimal initDeposit = 0;
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("------------CREATE ACUOUNT---------");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Account Name:");
            var accountName = Console.ReadLine();
            while (initDeposit < 999)
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("-----------INITIAL DEPOSIT---------");
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("Deposit Amount:");
                decimal initDepositRead;
                if (decimal.TryParse(Console.ReadLine(), out initDepositRead))
                {
                    if (initDepositRead > 999)
                    {
                        initDeposit = initDepositRead;
                        Console.WriteLine("Account Created");

                    }
                    else
                    {
                        Console.WriteLine("the minimum amount is R1000");
                    }
                }
                else
                {
                    Console.WriteLine("Please only insert the amount");
                }

            }
            _state = 0;

        }
        public static void DepositIntoAccount()
        {
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("---------DEPOSIT INTO ACCOUNT------");
            Console.WriteLine("-----------------------------------");
            IAccount account = null;
            while (account == null)
            {
                Console.WriteLine("Account id:");
                var accountId = Console.ReadLine();
                account = Accounts.Find(x => x.AccountUniqueId == accountId);
                if (account == null) Console.WriteLine("could not find the account");
            }
            Console.WriteLine("Amount to deposit:");
            var amountDeposited = Console.ReadLine();
            Console.WriteLine("Current Balance:" + account.Deposit(decimal.Parse(amountDeposited)));
            _state = 0;
        }
        public static void WithdrawFromAccount()
        {
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("--------WITHDRAW FROM ACCOUNT------");
            Console.WriteLine("-----------------------------------");
            IAccount account = null;
            while (account == null)
            {
                Console.WriteLine("Account id:");
                var accountId = Console.ReadLine();
                account = Accounts.Find(x => x.AccountUniqueId == accountId);
                if (account == null) Console.WriteLine("could not find the account");
            }
            Console.WriteLine("Amount to withdraw:");
            var amountwithdrawn = Console.ReadLine();
            try { Console.WriteLine("Current Balance:" + account.Withdraw(decimal.Parse(amountwithdrawn))); }
            catch (Exception e) { Console.WriteLine(e.Message); }
            _state = 0;
        }

    }
}
